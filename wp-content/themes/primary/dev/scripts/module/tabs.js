import $ from 'jquery';

export default () => {
  const $tabsContainer = $('.js-tabs');
  let tabsHead;

  if (window.innerWidth < 576) {
    tabsHead = $tabsContainer.find('.js-mob-tabs-button');

    $tabsContainer
      .find('.js-tabs-content')
      .css('display', 'none');

    $tabsContainer
      .find('.js-mob-tabs-button.is-active')
      .removeClass('is-active');

    changeMobileTabsHandler($tabsContainer, tabsHead);
  } else {
    tabsHead = $tabsContainer.find('.js-tabs-button');
    changeTabsHandler($tabsContainer, tabsHead);
  }

  tabsHead.css('display', 'block');
};

const changeTabsHandler = ($tabsContainer, tabsHead) => {
  tabsHead.click((event) => {
    const $target = $(event.currentTarget);
    const tabNumber = $target.attr('data-count');

    tabsHead.removeClass('is-active');
    $target.addClass('is-active');

    $tabsContainer.find('.js-tabs-content').fadeOut(500);

    setTimeout(() => {
      $(`.js-tabs-content[data-count="${tabNumber}"]`).fadeIn(500);
    }, 500);
  });
};

const changeMobileTabsHandler = ($tabsContainer, tabsHead) => {
  tabsHead.click((event) => {
    const $target = $(event.currentTarget);
    const $targetContent = $(`.js-tabs-content[data-count="${$target.attr('data-count')}"]`);

    tabsHead.not($target).removeClass('is-active');
    $target.toggleClass('is-active');

    $tabsContainer.find('.js-tabs-content').slideUp(500);
    if ($target.hasClass('is-active')) {
      $targetContent.slideDown(500);
    }
  });
};
