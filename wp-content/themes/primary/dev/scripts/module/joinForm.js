import $ from 'jquery';
import 'jquery-validation';

export default () => {
  const $joinForm = $('.js-join-form');

  $joinForm.submit((event) => {
    event.preventDefault();
    const $message = $joinForm.find('.js-response-message');

    if ($message.length) {
      $message.remove();
    }

    if ($joinForm.valid()) {
      getResponse($joinForm);
    }
  });
};

const getResponse = ($joinForm) => {
  const $emailInput = $joinForm.find('input');

  $.ajax({
    method: 'POST',
    url: '/wp-json/newsletter/v1/subscribe',
    dataType: 'json',
    data: {
      email: $emailInput.val(),
    },
    success: () => {
      $emailInput.after(`<p class="js-response-message"
                            style="margin-top: 15px;
                                   font-size: 20px;
                                   color: green;">
                            Thanks for join us!
                         </p>`);
    },
    error: (error) => {
      $emailInput.after(`<p class="js-response-message"
                            style="margin-top: 15px;
                                   font-size: 20px;
                                   color: red;">
                            ${error.responseJSON.message}
                         </p>`);
    },
  });
};
