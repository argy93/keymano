import $ from 'jquery';
import '@fancyapps/fancybox/dist/jquery.fancybox';
import '@fancyapps/fancybox/dist/jquery.fancybox.css';
import '../../../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min';
import '../../../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css';

export default () => {
  const videos = $('.c-videos');

  if (videos.length) {
    videos.mCustomScrollbar({
      axis: 'y',
    });
  }

  $('.js-iframe-popup').click((event) => {
    const $target = $(event.currentTarget);

    $.fancybox.open({
      src: $target.find('iframe').attr('src'),
      type: 'iframe',
    });
  });
};
