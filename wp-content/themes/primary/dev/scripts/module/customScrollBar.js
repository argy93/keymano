import $ from 'jquery';
import '../../../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min';
import '../../../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css';

export default () => {
  const articles = $('.c-articles__box');

  if (articles.length) {
    articles.mCustomScrollbar({
      axis: 'y',
    });
  }
};
