import $ from 'jquery';

export default () => {
  $('.c-mob-menu__close').click(() => {
    $('.c-mob-menu').removeClass('is-active');
  });

  $('.c-mob-icon').click(() => {
    $('.c-mob-menu').addClass('is-active');
  });
};
