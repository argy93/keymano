import $ from 'jquery';
import '../../../node_modules/jquery.cookie/jquery.cookie';

export default () => {
  const cookieLayout = $('.js-cookie');

  if ($.cookie('policy_accept')) {
    cookieLayout.css('display', 'none');
  }

  cookieLayout.find('.js-btn').click(() => {
    $.cookie('policy_accept', 'true', { expires: 365 });
    cookieLayout.css('display', 'none');
  });
};
