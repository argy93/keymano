import '../style/main.scss';
import $ from 'jquery';
import slickExample from './module/slick-example';
import mobileMenu from './module/mob-menu';
import cookie from './module/cookie';
import appearanceAnimation from './module/appearanceAnimation';
import tabs from './module/tabs';
import joinForm from './module/joinForm';
import videoPopup from './module/videoPopup';
import customScrollBar from './module/customScrollBar';

$(document).ready(() => {
  cookie();
  slickExample();
  mobileMenu();
  appearanceAnimation();
  tabs();
  joinForm();
  videoPopup();
  customScrollBar();
});
