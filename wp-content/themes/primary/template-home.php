<?php /* Template Name: Home page */

$context = Timber::context();
$timber_post = Timber::query_post();
$context['post'] = $timber_post;
$context['join'] = get_field('join_form', 'options');
Timber::render( array( 'template-home.twig', 'page.twig' ), $context );
