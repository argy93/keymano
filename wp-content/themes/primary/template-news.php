<?php /* Template Name: News page */

$context             = Timber::context();
$timber_post         = Timber::query_post();
$context['post']     = $timber_post;
$context['videos']     = Timber::get_posts(
  array(
    'post_type'      => 'videos',
    'posts_per_page' => - 1,
    'orderby'        => array(
      'date' => 'DESC'
    )
  )
);
$context['articles'] = Timber::get_posts(
  array(
    'post_type'      => 'articles',
    'posts_per_page' => - 1,
    'orderby'        => array(
      'date' => 'DESC'
    )
  )
);
$context['join'] = get_field('join_form', 'options');
Timber::render( array( 'template-news.twig', 'page.twig' ), $context );
