<?php /* Template Name: Business page */

$context = Timber::context();
$timber_post = Timber::query_post();
$context['post'] = $timber_post;
Timber::render( array( 'template-business.twig', 'page.twig' ), $context );
