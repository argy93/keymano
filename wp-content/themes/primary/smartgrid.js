// To generate a grid just write "node smartgrid.js" in your console

const smartgrid = require('smart-grid');

const settings = {
  filename: 'grid',
  outputStyle: 'scss',
  tab: '  ',
  columns: 12,
  offset: '30px',
  container: {
    maxWidth: '1418px',
    fields: '15px',
  },
  breakPoints: {
    xl: {
      width: '1440px',
      fields: '15px',
    },
    lg: {
      width: '1280px',
      fields: '15px',
    },
    md: {
      width: '991px',
      fields: '15px',
    },
    sm: {
      width: '767px',
      fields: '10px',
    },
    xs: {
      width: '575px',
      fields: '10px',
    },
    xxs: {
      width: '320px',
      fields: '10px',
    },
  },
};

smartgrid('./dev/style/vendor', settings);
