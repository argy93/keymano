<?php
/**
 * Timber starter-theme
 * https://github.com/timber/starter-theme
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
	});

	return;
}

/**
 * Sets the directories (inside your theme) to find .twig files
 */
Timber::$dirname = array( 'templates', 'views' );

/**
 * By default, Timber does NOT autoescape values. Want to enable Twig's autoescape?
 * No prob! Just set this value to true
 */
Timber::$autoescape = false;


/**
 * We're going to configure our theme inside of a subclass of Timber\Site
 * You can move this to its own file and include here via php's include("MySite.php")
 */
class StarterSite extends Timber\Site {
	/** Add timber support. */
	public function __construct() {
    add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_static' ));
		add_action( 'after_setup_theme', array( $this, 'theme_supports' ) );
    add_action( 'customize_register', array($this, 'wp_customize') );
		add_filter( 'timber/context', array( $this, 'add_to_context' ) );
		add_filter( 'timber/twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		add_action( 'init', array( $this, 'acf' ) );
		parent::__construct();
	}
	/** This is where you can register custom post types. */
	public function register_post_types() {
    register_post_type( 'videos',
      array(
        'labels' => array(
          'name' => __( 'Videos' ),
          'singular_name' => __( 'Video' )
        ),
        'description' => 'Use this posts in News page',
        'public' => true,
        'has_archive' => false,
        'publicly_queryable' => false,
        'supports' => ['title', 'excerpt', 'custom-fields', 'page-attributes']
      )
    );

    register_post_type( 'articles',
      array(
        'labels' => array(
          'name' => __( 'Articles' ),
          'singular_name' => __( 'Article' )
        ),
        'description' => 'Outside post',
        'public' => true,
        'has_archive' => false,
        'publicly_queryable' => false,
        'supports' => ['title', 'thumbnail', 'custom-fields', 'page-attributes']
      )
    );
	}
	/** This is where you can register custom taxonomies. */
	public function register_taxonomies() {

	}

	public function acf() {
    acf_add_options_page('Theme Settings');
  }

	public function wp_customize($wp_customize) {

    $wp_customize->add_section( 'primary_socials' , array(
      'title'      => __( 'Socials', 'primary' ),
      'priority'   => 30,
    ) );

    $wp_customize->add_setting( 'logo_in_footer' );
    $wp_customize->add_setting( 'social_fb' );
    $wp_customize->add_setting( 'social_tw' );
    $wp_customize->add_setting( 'social_in' );
    $wp_customize->add_setting( 'social_ln' );
    $wp_customize->add_setting( 'social_medium' );
    $wp_customize->add_setting( 'copyright' );
    $wp_customize->add_setting( 'dev' );

    $wp_customize->add_control('social_fb', array(
      'label' => __( 'Facebook', 'primary' ),
      'section' => 'primary_socials',
      'type' => 'url'
    ));

    $wp_customize->add_control('social_tw', array(
      'label' => __( 'Twitter', 'primary' ),
      'section' => 'primary_socials',
      'type' => 'url'
    ));

    $wp_customize->add_control('social_in', array(
      'label' => __( 'Instagram', 'primary' ),
      'section' => 'primary_socials',
      'type' => 'url'
    ));

    $wp_customize->add_control('social_ln', array(
      'label' => __( 'LinkedIn', 'primary' ),
      'section' => 'primary_socials',
      'type' => 'url'
    ));

    $wp_customize->add_control('social_medium', array(
      'label' => __( 'Medium', 'primary' ),
      'section' => 'primary_socials',
      'type' => 'url'
    ));

    $wp_customize->add_control('copyright', array(
      'label' => __( 'Copyright', 'primary' ),
      'section' => 'title_tagline',
      'priority' => 11,
      'type' => 'text'
    ));

    $wp_customize->add_control('dev', array(
      'label' => __( 'Develop', 'primary' ),
      'section' => 'title_tagline',
      'priority' => 11,
      'type' => 'text'
    ));

    $wp_customize->add_control(
      new WP_Customize_Image_Control(
        $wp_customize,
        'logo',
        array(
          'label'      => __( 'Footer logo', 'primary' ),
          'section'    => 'title_tagline',
          'settings'   => 'logo_in_footer',
          'priority' => 9
        )
      )
    );

  }

	public function enqueue_static() {
	  $version = StarterSite::getVersion();

    wp_enqueue_style( 'style-main', get_template_directory_uri() . '/static/main-'.$version.'.css', array(), '1.0.0' );

    wp_enqueue_script('jquery');
    wp_enqueue_script( 'script-main', get_template_directory_uri() . '/static/main-'.$version.'.js', array('jquery'), '1.0.0', true );
  }

	/** This is where you add some context
	 *
	 * @param string $context context['this'] Being the Twig's {{ this }}.
	 */
	public function add_to_context( $context ) {
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );

    $context['logo'] = $image[0];
    $context['logo_footer'] = get_theme_mod( 'logo_in_footer' );
		$context['menu'] = new Timber\Menu('main');
    $context['menu_auth'] = new Timber\Menu('auth');
    $context['menu_footer'] = new Timber\Menu('footer');
    $context['menu_policy'] = new Timber\Menu('policy');
    $context['socials'] = array(
      'fb' => get_theme_mod( 'social_fb' ),
      'tw' => get_theme_mod( 'social_tw' ),
      'in' => get_theme_mod( 'social_in' ),
      'ln' => get_theme_mod( 'social_ln' ),
      'medium' => get_theme_mod( 'social_medium' )
    );
    $context['copyright'] = get_theme_mod( 'copyright' );
    $context['dev'] = get_theme_mod( 'dev' );
    $context['policy_banner'] = get_field('policy_banner', 'options');
		$context['site'] = $this;
		return $context;
	}

	public function theme_supports() {
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5', array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
		add_theme_support(
			'post-formats', array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'audio',
			)
		);

		add_theme_support( 'menus' );
    add_theme_support( 'custom-logo');
	}

	/** This Would return 'foo bar!'.
	 *
	 * @param string $text being 'foo', then returned 'foo bar!'.
	 */
	public function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	/** This is where you can add your own functions to twig.
	 *
	 * @param string $twig get extension.
	 */
	public function add_to_twig( $twig ) {
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter( new Twig_SimpleFilter( 'myfoo', array( $this, 'myfoo' ) ) );
		return $twig;
	}

	public static function getVersion() {
    $version = @file_get_contents(get_template_directory().'/version.md5');

    if (!$version) return 1;

    return $version;
  }
}

new StarterSite();
