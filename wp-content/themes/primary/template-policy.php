<?php /* Template Name: Policy page */

$context = Timber::context();
$timber_post = Timber::query_post();
$context['post'] = $timber_post;
Timber::render( array( 'template-policy.twig', 'page.twig' ), $context );
